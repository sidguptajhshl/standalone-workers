# Coverage code
require 'simplecov'
require 'simplecov-console'
require 'rspec'
require 'factory_girl'
require 'pathname'
require 'rack/test'

SimpleCov.start do
  add_filter '/spec/'
  add_group 'core/'
  add_group 'Crons', 'crons/'
end

SimpleCov.formatters = [
  SimpleCov::Formatter::HTMLFormatter,
  SimpleCov::Formatter::Console
]
# Standard require starts here

require 'healthloop/workers'

ENV['RACK_ENV'] = 'test'
RSpec.configure do |config|
  # EXPECT SETTINGS
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
    expectations.syntax = :expect # Disable `should`
  end

  # MOCK SETTINGS
  config.mock_with :rspec do |mocks|
    mocks.syntax = :expect # Disable `should_receive` and `stub`
    mocks.verify_partial_doubles = true
  end

  # FACTORY GIRL
  config.include FactoryGirl::Syntax::Methods
  config.before(:suite) do
    FactoryGirl.find_definitions
  end
end