require 'healthloop/workers/configuration'
require 'healthloop/workers/logging'
require 'healthloop/workers/factory'

module HealthLoop
  module Workers
    class << self
      include HealthLoop::Workers::Logging

      def start(options)
        Process.daemon(nochdir: true) if options[:daemonize]
        HealthLoop::Workers.configure_using_file(options[:config])
        worker_name = options[:worker_name]
        worker_config = HealthLoop::Workers.configuration.workers.with_indifferent_access[worker_name]
        start_worker(worker_name,  worker_config)
      end

      def start_worker(worker_name, worker_config)
        @worker = Factory.create(worker_name, worker_config)
        logger.info('Starting worker')
        @worker.validate_and_start
      end
    end
  end
end
