require 'logging'
require 'healthloop/workers/extensions'

module HealthLoop
  module Workers
    module Logging
      def self.logger(klass = fully_qualified_class_name)
        return ::Logging::Repository.instance[klass] if ::Logging::Repository.instance.has_logger? klass
        ::Logging.logger[klass].tap do |lggr|
          lggr.add_appenders ::Logging.appenders.stdout
        end
      end
      
      def logger(klass = fully_qualified_class_name)
        HealthLoop::Workers::Logging.logger(klass)
      end
    end
  end
end

::Logging.color_scheme('bright',
                       levels: {
                         info: :green,
                         warn: :yellow,
                         error: :red,
                         fatal: %i[white on_red]
                       },
                       date: :blue,
                       logger: :cyan,
                       message: :white)

::Logging.appenders.stdout(
  layout: ::Logging.layouts.pattern(
    pattern: '[%d] %-5l %c: %m\n',
    color_scheme: 'bright'
  ),
  backtrace: true
)
