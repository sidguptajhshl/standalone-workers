require 'virtus'
require 'active_model'

module HealthLoop
  module Workers
    module Configuration
      class MailConfig
        include Virtus.model
        include ActiveModel::Validations

        attribute :address, String
        attribute :port, Integer
        attribute :username, String
        attribute :password, String
        attribute :enable_ssl, Virtus::Attribute::Boolean, default: true
        validates_presence_of :address, :port, :username, :password
      end
    end
  end
end