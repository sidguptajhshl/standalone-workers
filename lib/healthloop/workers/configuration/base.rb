require 'virtus'
require 'active_model'
require 'healthloop/workers/configuration/mail'
require 'active_support/core_ext/hash'

module HealthLoop
  module Workers
    module Configuration
      class Base
        include Virtus.model
        include ActiveModel::Validations

        attribute :workers, ActiveSupport::HashWithIndifferentAccess, default: {}
        attribute :http_retry_interval, Integer, default: 10
        attribute :http_retry_count, Integer, default: 3
        attribute :standard_retry_interval, Integer,default: 10
        attribute :standard_retry_count, Integer, default: 3
        attribute :mail, MailConfig, default: MailConfig.new
        attribute :disable_mails, Virtus::Attribute::Boolean, default: false
        
        validate :check_mail_config

        def disable_mails?
          disable_mails
        end

        def check_mail_config
          return if disable_mails?
          mail.validate
          errors.add('mail', mail.errors.full_messages.to_yaml) unless mail.valid?
        end
      end
    end
  end
end