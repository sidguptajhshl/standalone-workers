require 'healthloop/workers/configuration/base'
require 'erb'

module HealthLoop
  module Workers
    class << self
      def configuration
        raise 'Need to configure the application first' unless @configuration
        @configuration
      end

      def configure_using_file(file_path)
        @config_file = file_path
        config_hash = YAML.load(ERB.new(File.read(@config_file)).result)
        @configuration = Configuration::Base.new(config_hash)
        finalize_config
        @configuration
      end

      def configure
        @configuration = Configuration::Base.new
        yield @configuration
        finalize_config
        return @configuration
      end

      private 

      def finalize_config
        conf = HealthLoop::Workers.configuration
        conf.validate!
        if conf.disable_mails?
          Mail.defaults { delivery_method :test }
        else
          Mail.defaults do
            delivery_method :smtp,  address: HealthLoop::Workers.configuration.mail.address,
                                    port: HealthLoop::Workers.configuration.mail.port,
                                    user_name: HealthLoop::Workers.configuration.mail.username,
                                    password: HealthLoop::Workers.configuration.mail.password,
                                    enable_ssl: HealthLoop::Workers.configuration.mail.enable_ssl
          end
        end
      end
    end
  end
end
