require 'healthloop/workers/logging'
require 'healthloop/workers/core/status'

module HealthLoop
  module Workers
    class Worker
      include HealthLoop::Workers::Logging

      attr_accessor :status, :errors, :messages

      WORKER_STOP_TIMEOUT = 20
      MAX_MESSAGES_SIZE = 500
      MAX_ERRORS_SIZE = 500

      def initialize(args)
        @args = args
        @target_status = Status::STOPPED
        @status = Status::STOPPED
        @thread = nil
        @errors = []
        @messages = []
      end

      def running?
        return false if @thread.nil?
        @thread.alive?
      end

      def keep_running?
        @target_status == Status::RUNNING
      end

      def validate_and_start
        validate
        start
      end

      def context
        instance_variables.map do |attribute|
          { attribute => instance_variable_get(attribute) }
        end
      end

      private

      def validate
        logger.warn('Please override #validate method in your worker to add some actual validations')
      end

      def process(_args)
        logger.warn('Please override #process method in your worker to add some actual validations')
      end

      def start
        validate
        @target_status = Status::RUNNING
        logger.info("Creating worker #{self.class}")
        perform(@args.clone)
        logger.info("Started worker #{self.class}")
        @status = Status::RUNNING
      end

      def add_error(e, *params)
        error_hash = { time: DateTime.now, message: e.message, backtrace: e.backtrace, cool_stuff: e.prettify, data: params }
        logger.error(error_hash)
        @errors << error_hash
        @errors.shift if @errors.size == MAX_ERRORS_SIZE
      end

      def add_message(message, *params)
        message_hash = { time: DateTime.now, message: message, data: params }
        logger.info(message_hash)
        @messages << message_hash
        @messages.shift if @messages.size == MAX_MESSAGES_SIZE
      end

      def perform(args)
        if keep_running?
          process(args)
          logger.info("Running worker #{self.class}")
        else
          @status = Status::STOPPED
          logger.info("Marked worker #{self.class} as stopped.")
        end
      end
    end
  end
end
