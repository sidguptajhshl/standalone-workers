module HealthLoop
  module Workers
    module Status
      STOPPED = 'Stopped'.freeze
      RUNNING = 'Running'.freeze
    end
  end
end
