require 'healthloop/workers/core/worker'
module HealthLoop
  module Workers
    # CRON base class
    class Cron < Worker
      def initialize(args)
        @time = args[:time] || 600
        super
      end

      def perform(args)
        loop do
          super
          logger.info("Keep Running? #{keep_running?}. Sleep time #{@time}")
          break unless keep_running?
          sleep(@time)
        end
      end

      def validate
        logger.warn('Please override #validate method in your worker(and call super) to add some actual validations')
        raise 'Invalid Time' unless @time > 0
      end
    end
  end
end
