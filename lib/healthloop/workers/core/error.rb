module healthloop
  module Workers
    class HealthLooploopError < StandardError
      attr_reader :params

      def initialize(message, params)
        super(message)
        @params = params
      end
    end
  end
end
