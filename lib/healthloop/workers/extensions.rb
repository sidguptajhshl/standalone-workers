require 'awesome_print'
class Object
  def prettify(_out = $DEFAULT_OUTPUT, _width = 80)
    ai
      # rubocop:disable Lint/RescueException
  rescue Exception
    inspect
  end

  def fully_qualified_class_name
    self.class.name
  end

  def self.fully_qualified_class_name
    self.name
  end
end

class Module
  def fully_qualified_class_name
    self.name
  end

  def self.fully_qualified_class_name
    self.name
  end
end

AwesomePrint.defaults = {
    indent:        4,
    index:         false,
    html:          false,
    multiline:     true,
    plain:         true,
    raw:           true,
    sort_keys:     false,
    limit:         false, # Limit arrays & hashes. Accepts bool or int.
    ruby19_syntax: true, # Use Ruby 1.9 hash syntax in output.
    color: {
        args:       :pale,
        array:      :white,
        bigdecimal: :blue,
        class:      :yellow,
        date:       :greenish,
        falseclass: :red,
        fixnum:     :blue,
        float:      :blue,
        hash:       :pale,
        keyword:    :cyan,
        method:     :purpleish,
        nilclass:   :red,
        rational:   :blue,
        string:     :yellowish,
        struct:     :pale,
        symbol:     :cyanish,
        time:       :greenish,
        trueclass:  :green,
        variable:   :cyanish
    }
}
