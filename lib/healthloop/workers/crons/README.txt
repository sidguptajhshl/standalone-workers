# CREATE A FILE WITH STRUCTURE LIKE THIS IN HERE.

###########################################################################
        require_relative '../core/cron'

        module HealthLoop
          module Crons
            class YourCronName < healthloop::Workers::Cron
              def process(*args)
                //your code here
              end
            end
          end
        end

############################################################################

# ADD THE CRON TO CRON_MAP variable

# Add your config in the config file with a separate section for your cron with the label name

# If you are adding blocking processes in your cron(which you shouldn't) then make sure that you keep checking for the variable keep_running? after every iteration.

# DO NOT CATCH AND LOG ERRORS IN THE CRONS UNLESS YOU ARE IMPLEMENTING SOME BUSINESS LOGIN ON THAT EXCEPTION. ALL EXCEPTIONS ARE CAUGHT IN THE PARENT LIBRARY
