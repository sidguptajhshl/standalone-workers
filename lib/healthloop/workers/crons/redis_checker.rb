require 'redis'
require 'mail'
require 'timeout'
require 'healthloop/workers/core/cron'
require 'healthloop/workers/retriable'

module HealthLoop
  module Crons
    class RedisChecker < HealthLoop::Workers::Cron
      include HealthLoop::Workers::Retriable
      def process(args)
        @redis_url = args[:redis_url]
        @to_address = args[:to_address] #|| 'redisalerts@healthloop.com'
        @from_address = args[:from_address]# || 'alerts@healthloop.com'
        logger.info "Running Redis Checker on #{@redis_url}"
        Timeout.timeout(100) { perform_checks }
      rescue Timeout::Error => e
        logger.error('Timed out trying to perform a check on Redis')
      end

      def perform_checks
        redis_client = redis = Redis.new(url: @redis_url)
        ping_check(redis_client) && logger.info('PING SUCCEEDED')
        set_get_check(redis_client) && logger.info('SET AND GET WORKED FOR SIMPLE VALUES')
        incr_decr_check(redis_client, 50+rand(100), rand(50)) && logger.info('INCR AND DECR WORKED')
      rescue Redis::CannotConnectError => e
        notify("FAILED TO CONNECT TO SERVER. ERROR: #{e}")
      rescue => e
        notify(e)
      end

      def ping_check(client)
        ping_response = client.ping
        return true if ping_response == 'PONG'
        notify("Ping has failed. Ping reply expected was PONG. Got: #{ping_response}")
        return false
      end

      def set_get_check(client)
        100.times do
          random_key = "redis-checker-#{rand}"
          random_value = rand.to_s
          client.set(random_key, random_value, seconds: 2)
          stored_value = client.get(random_key)
          return true if stored_value == random_value
          notify("Failed to receive correct value from redis after set. Expected #{random_value}. Got #{stored_value}")
          return false
          sleep 0.5
        end
      end

      def incr_decr_check(client, incr_count, decr_count)
        counter_key = "redis-checker-#{rand}"
        client.set("redis-checker-counter", 0,seconds: 2)
        incr_count.times {client.incr(counter_key)}
        incr_value = client.get(counter_key)
        if incr_value.to_i != incr_count
          notify("Failed to correctly incr value. Expected: #{incr_count}. Got: #{incr_value}")
          return false
        end
        decr_count.times { client.decr(counter_key) }
        decr_value = client.get(counter_key)
        if decr_value.to_i != incr_count-decr_count
          notify("Failed to correctly decr value. Expected: #{decr_count}. Got: #{decr_value}")         
          return false 
        end
        return true
      end

      def notify(message, status: :failed)
        logger.error(message) if status == :failed
        logger.info(message) if status != :failed
        mail_body = "Redis Server: #{@redis_url}. Status: #{status}. Message: #{message}"
        send_mail(@to_address, @from_address, 'RedisChecker: Unhandled Error', mail_body)
      end

      def send_mail(to_address, from_address, mail_subject, mail_body)
        execute_with_net_http_retry do
          mail = Mail.new do
            to to_address
            from from_address
            subject mail_subject
            body mail_body
          end
          logger.info(mail.to_s)
          mail.deliver!
        end
      end
    end
  end
end