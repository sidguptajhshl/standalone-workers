require 'retriable'
require 'net/http'

module HealthLoop
  module Workers
    # Retry mechanism with exponential back-off for different types of communication
    module Retriable
      include HealthLoop::Workers::Logging
      NET_HTTP_RETRIABLE_ERRORS = [Net::HTTPRetriableError, Net::HTTPRequestTimeOut,
                                  Net::HTTPGatewayTimeOut, Errno::ECONNRESET].freeze

      def execute_with_net_http_retry(options = {})
        interval = options[:interval] || HealthLoop::Workers.configuration.http_retry_interval
        tries = options[:tries] || HealthLoop::Workers.configuration.http_retry_count
        ::Retriable.retriable(on_retry: retry_logging_block, on: NET_HTTP_RETRIABLE_ERRORS,
                              tries: tries, base_interval: interval) do
          yield
        end
      end

      def execute_with_standard_retry(options = {})
        interval = options[:interval] || HealthLoop::Workers.configuration.standard_retry_interval
        tries = options[:tries] || HealthLoop::Workers.configuration.standard_retry_count
        ::Retriable.retriable(on_retry: retry_logging_block, on: StandardError,
                              tries: tries, base_interval: interval) do
          yield
        end
      end

      def retry_logging_block
        proc do |exception, try, elapsed_time, next_interval|
          logger.warn("#{exception.class}: '#{exception.message}' - #{try} tries in #{elapsed_time}" \
            " seconds and #{next_interval} seconds until the next try.")
        end
      end
    end
  end
end