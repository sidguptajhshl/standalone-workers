module HealthLoop
  # VERSION FILE
  module Workers
    def self.version
      if ENV['BUILD_NUMBER'].nil? || ENV['BUILD_NUMBER'].empty?
        '0.0.0'
      else
        suffix = ENV['BRANCH_NAME'] == ENV['STABLE_BRANCH'] ? '' : '.pre'
        "0.1.#{ENV['BUILD_NUMBER'].to_i}#{suffix}"
      end
    end
    VERSION = version
  end
end
