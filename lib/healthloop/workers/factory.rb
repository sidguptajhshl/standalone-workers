Dir.glob("#{File.dirname(__FILE__)}/crons/*.rb", &method(:require))
require 'healthloop/workers/logging'

module HealthLoop
  module Workers
    # Create a valid worker based on name
    module Factory
      class << self
        include HealthLoop::Workers::Logging
        
        WORKER_MAP = { redis_checker: HealthLoop::Crons::RedisChecker }.freeze

        def create(worker_name, worker_config)
          raise 'Invalid Worker Name' unless WORKER_MAP.include?(worker_name)
          WORKER_MAP[worker_name].new(worker_config)
        end
      end
    end
  end
end
