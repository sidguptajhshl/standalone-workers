FROM ruby:2.4.3

RUN gem install foreman

COPY Gemfile* /bundle/
ENV BUNDLE_GEMFILE=/bundle/Gemfile \
    BUNDLE_JOBS=2 \
    BUNDLE_PATH=/bundle

RUN bundle install --without development test --deployment

COPY . /app/
WORKDIR /app

CMD foreman start $WORKER_NAME
